package collector

import (
	"fmt"
	"os"
)

// some shortcuts:

func (c *teamsCollector) fatalf(format string, a ...interface{}) {
	fmt.Fprintf(c.errTo, format, a...)
	os.Exit(1)
}

func (c *teamsCollector) errorf(format string, a ...interface{}) {
	fmt.Fprintf(c.errTo, format, a...)
}

func (c *teamsCollector) printf(format string, a ...interface{}) {
	fmt.Fprintf(c.outTo, format, a...)
}
