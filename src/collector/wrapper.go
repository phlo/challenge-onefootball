package collector

import (
	"fmt"
	"os"

	"github.com/gocolly/colly"
	"gitlab.com/phl0/challenge-onefootball/src/data"
	"gitlab.com/phl0/challenge-onefootball/src/parser"
)

// teamsCollector wraps the logic (of crawling for a set of teams) around a colly teamsCollector
type teamsCollector struct {
	colly.Collector
	mask    string
	teams   []string
	verbose bool
	outTo   *os.File
	errTo   *os.File

	data data.Storage
}

// NewCollector will instantiate the collector
func NewCollector(
	playersStorage data.Storage,
	mask string,
	cachePath string,
	teams []string,
	politeness []*colly.LimitRule,
	verbose bool,
	outTo, errTo *os.File,
) *teamsCollector {
	collyCollector := newCollyCollector(cachePath, politeness...)

	c := &teamsCollector{
		Collector: *collyCollector,
		mask:      mask,
		teams:     teams,
		verbose:   verbose,
		outTo:     outTo,
		errTo:     errTo,
		data:      playersStorage,
	}

	c.OnRequest(func(r *colly.Request) {
		if verbose {
			c.printf("> %s\n", r.URL)
		}
	})

	c.OnResponse(func(r *colly.Response) {
		var (
			team *parser.Team
			err  error
		)

		if team, err = parser.ExtractTeam(r.Body); err != nil {
			if verbose {
				c.errorf("* %s:\n  can't extract team from response body:\n  %s", r.Request.URL, string(r.Body))
			}
			return
		}

		if verbose {
			c.printf("> %s %s\n", r.Request.URL, team.Name)
		}

		if !c.isInteresting(team) {
			return
		}

		c.unwatchTeam(team.Name)

		for _, player := range team.Players {
			if err = c.data.Add(player, team); err != nil {
				c.errorf("* error adding player %s to team %s: %s", player.Name, team.Name, err)
			}
		}
	})

	return c
}

// CrawlOnefootball will incrementally crawl mask links until either all teams are found
// or the limit (which can be 0 for infinite) is reached.
func (c *teamsCollector) CrawlOnefootball(maxLimit int) {
	var (
		currentId    = 0
		currentBatch = 0
		batchSize    = 20
	)

	if maxLimit > 0 && maxLimit < batchSize {
		batchSize = maxLimit
	}

	for len(c.teams) > 0 && (maxLimit == 0 || currentId < maxLimit) {
		if c.verbose {
			c.printf("* starting batch %d\n", currentBatch+1)
			c.printf("* remaining teams: %v\n", c.teams)
		}

		for i := 0; i < batchSize; i++ {
			currentId = currentBatch*batchSize + i + 1
			targetURL := fmt.Sprintf(c.mask, currentId)
			if err := c.Visit(targetURL); err != nil {
				c.fatalf("error while visiting %s: %s", targetURL, err)
			}
			if currentId == maxLimit {
				break
			}
		}

		currentBatch++

		c.Wait()
	}

	if currentId == maxLimit && c.verbose {
		c.printf("* limit reached: %d\n", maxLimit)
	}
}

func (c *teamsCollector) isInteresting(team *parser.Team) bool {
	for _, configTeamName := range c.teams {
		if configTeamName == team.Name {
			return true
		}
	}
	return false
}

func (c *teamsCollector) unwatchTeam(teamName string) {
	for i, team := range c.teams {
		if teamName == team {
			c.teams = append(c.teams[:i], c.teams[i+1:]...)
		}
	}
}
