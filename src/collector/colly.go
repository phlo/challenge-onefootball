package collector

import (
	"net"
	"net/http"
	"time"

	"github.com/gocolly/colly"
	"github.com/gocolly/colly/extensions"

	log "github.com/sirupsen/logrus"
)

func newCollyCollector(cachePath string, limits ...*colly.LimitRule) *colly.Collector {
	c := colly.NewCollector(
		colly.MaxDepth(1),
		colly.Async(true),
		colly.CacheDir(cachePath),
	)
	extensions.RandomUserAgent(c)

	c.WithTransport(&http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   7 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	})

	for _, limit := range limits {
		if err := c.Limit(limit); err != nil {
			log.WithError(err).Fatal("failed while setting collector limit")
		}
	}

	return c
}
