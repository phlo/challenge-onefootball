package parser

import (
	"io/ioutil"
	"strings"
	"testing"

	"github.com/go-test/deep"
)

func TestExtractTeam(t *testing.T) {
	var (
		content []byte
		err     error
	)
	if content, err = ioutil.ReadFile("test-data/turkey-123.json"); err != nil {
		t.Fatalf("can't load mock content")
	}

	type args struct {
		content []byte
	}
	tests := []struct {
		name    string
		args    args
		want    *Team
		wantErr bool
	}{
		{
			name: "simple",
			args: args{content: content},
			want: &Team{
				Id:   123,
				Name: "Turkey",
				Players: []*Player{
					{
						Id:   "201022",
						Name: "Merih Demiral",
						Age:  "21",
					},
					{
						Id:   "24999",
						Name: "Hakan Calhanoglu",
						Age:  "25",
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ExtractTeam(tt.args.content)
			if (err != nil) != tt.wantErr {
				t.Errorf("ExtractTeam() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Errorf("diff:\n%s\n", strings.Join(diff, "\n"))
			}
		})
	}
}
