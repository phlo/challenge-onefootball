package parser

import "strings"

func TeamsStr(teams []*Team) string {
	var teamStrings []string
	for _, team := range teams {
		teamStrings = append(teamStrings, team.Name)
	}
	return strings.Join(teamStrings, ", ")
}
