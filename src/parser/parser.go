package parser

import (
	"encoding/json"
	"strconv"

	"github.com/pkg/errors"
)

type response struct {
	Data struct {
		Team *Team
	}
}

type Team struct {
	Id      int
	Name    string
	Players []*Player
}

type Player struct {
	Id   string
	Name string
	Age  string
}

func ExtractTeam(content []byte) (*Team, error) {
	r := &response{}
	if err := json.Unmarshal(content, r); err != nil {
		return nil, errors.Wrap(err, "error unmarshaling json")
	}
	return r.Data.Team, nil
}

// GetIdInt returns 0 if the string id was invalid or its int equivalent otherwise
func (p *Player) GetIdInt() (id int) {
	id, err := strconv.Atoi(p.Id)
	if err != nil {
		id = 0
	}
	return
}
