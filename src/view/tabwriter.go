package view

import (
	"fmt"
	"os"
	"text/tabwriter"

	"gitlab.com/phl0/challenge-onefootball/src/data"
	"gitlab.com/phl0/challenge-onefootball/src/parser"
)

// Tabwriter implements Viewer
type Tabwriter struct{}

// Show shows a storage with fields separated by tabs
func (f Tabwriter) Show(out *os.File, storage data.Storage) {
	w := tabwriter.NewWriter(out, 0, 0, 5, ' ', tabwriter.AlignRight)
	fmt.Fprintf(w, "\t%s\t%s\t%s\t\n", "Name", "Age", "Teams")

	index := 1
	storage.Each(func(player *parser.Player, teams []*parser.Team) {
		fmt.Fprintf(w, "%d\t%s\t%s\t%s\t\n", index, player.Name, player.Age, parser.TeamsStr(teams))
		index++
	}, nil)

	w.Flush()
}
