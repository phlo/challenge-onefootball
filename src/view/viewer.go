package view

import (
	"os"

	"gitlab.com/phl0/challenge-onefootball/src/data"
)

// Viewer defines something to show a storage
type Viewer interface {
	Show(out *os.File, storage data.Storage)
}
