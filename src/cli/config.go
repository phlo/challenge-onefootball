package cli

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"unicode"
)

// ConfigOpts stores all configuration options
type Options struct {
	Verbose bool
	Cache   string
	UrlMask string
	Teams   []string
	Limit   int
}

var Config Options

func init() {
	Config = Options{}

	flag.BoolVar(&Config.Verbose, "v", false, "verbose output")
	flag.StringVar(&Config.Cache, "cache", ".cache", "cache path")
	flag.StringVar(&Config.UrlMask, "mask", "https://vintagemonster.onefootball.com/api/teams/en/%d.json",
		"url mask to use. %d will be replaced by ids")
	flag.IntVar(&Config.Limit, "limit", 100, "sets the upper limit for incrementing ids in links. "+
		"Crawling stops when either this is reached or all the interesting teams were found")

	addTeamsFlag()

	flag.Usage = func() {
		fmt.Fprint(flag.CommandLine.Output(), `This will do GET requests to the -mask param incrementing %d from 1 until it finds all the teams in the -teams list.
Then it will print all the players found  ordered alphabetically with their team(s) shown next to them`)
		fmt.Fprintf(flag.CommandLine.Output(), "\n\nUsage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}

	flag.Parse()
}

func addTeamsFlag() {
	var teamsInput string
	flag.StringVar(
		&teamsInput,
		"teams",
		"Germany, England, France, Spain, Manchester United, Arsenal, Chelsea, Barcelona, Real Madrid, Bayern Munich",
		"teams to look for, separated by commas")

	for _, inputTeam := range strings.Split(teamsInput, ",") {
		trimmed := strings.TrimFunc(inputTeam, func(r rune) bool {
			return unicode.IsSpace(r)
		})
		Config.Teams = append(Config.Teams, trimmed)
	}
}
