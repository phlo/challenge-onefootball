package data

import "gitlab.com/phl0/challenge-onefootball/src/parser"

// Storage is the interface to be implemented by a player storage. It corresponds to the requirements.
type Storage interface {
	// Add adds a player to a team
	Add(player *parser.Player, team *parser.Team) error

	// Each runs a callback on each player. Order (given by less) should default to alphabetical.
	Each(
		do func(player *parser.Player, teams []*parser.Team),
		sortingLessFn func(player1, player2 *parser.Player) bool,
	)
}
