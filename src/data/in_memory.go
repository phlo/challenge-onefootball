package data

import (
	"errors"
	"sort"
	"sync"

	"gitlab.com/phl0/challenge-onefootball/src/parser"
)

// InMemory holds data and exposes a minimum set of operations (like add)
// This can be tuned here, if needed, for better performance.
// todo unit tests and benchmarks.
type InMemory struct {
	players  map[int]*playerWithTeams
	sortable []*playerWithTeams // this slice of refs shouldn't be too expensive and will help with sorting
}

var lock = &sync.RWMutex{}

// playerWithTeams decorates parser's json decoded Player with teams
type playerWithTeams struct {
	*parser.Player
	Teams []*parser.Team
}

// Add adds a json unmarshaled player to the collection (or adds to an existing one's teams)
func (s *InMemory) Add(player *parser.Player, team *parser.Team) error {
	var id int
	if id = player.GetIdInt(); id == 0 {
		return errors.New("couldn't convert player id to int")
	}

	lock.Lock()
	defer lock.Unlock()

	// todo this instantiation could be moved to a mandatory constructor of a private type:
	if s.players == nil {
		s.players = map[int]*playerWithTeams{}
	}

	wrapped, exists := s.players[id]
	if !exists {
		newPlayer := &playerWithTeams{player, []*parser.Team{team}}
		s.players[id] = newPlayer
		s.sortable = append(s.sortable, newPlayer)

		return nil
	}

	wrapped.Teams = append(wrapped.Teams, team)

	return nil
}

// Each runs a callback on every player.
// Order is given by the compareLess func. When nil, it defaults to alphabetical.
func (s *InMemory) Each(
	do func(player *parser.Player, teams []*parser.Team),
	less func(player1, player2 *parser.Player) bool,
) {
	if less == nil {
		// defaults to alphabetical order
		less = func(player1, player2 *parser.Player) bool {
			return player1.Name < player2.Name
		}
	}

	sort.Slice(s.sortable, func(i, j int) bool {
		return less(s.sortable[i].Player, s.sortable[j].Player)
	})

	for _, player := range s.sortable {
		do(player.Player, player.Teams)
	}
}
