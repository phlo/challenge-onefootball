package main

import (
	"os"
	"time"

	"github.com/gocolly/colly"
	"gitlab.com/phl0/challenge-onefootball/src/cli"
	"gitlab.com/phl0/challenge-onefootball/src/collector"
	"gitlab.com/phl0/challenge-onefootball/src/data"
	"gitlab.com/phl0/challenge-onefootball/src/view"
)

// I don't have more time for tests, but basically this is made of swappable parts (even output) that can be mocked and tested
func main() {
	// this will store the players
	storage := &data.InMemory{}

	// This will politely crawl onefootball links and fill in the storage with players from the interesting teams.
	// Every response is cached so calls will not be performed twice.
	collector.NewCollector(
		storage,
		cli.Config.UrlMask,
		cli.Config.Cache,
		cli.Config.Teams,
		[]*colly.LimitRule{{
			Delay:       3 * time.Second,
			RandomDelay: 3 * time.Second,
			Parallelism: 4,
			DomainGlob:  "*.onefootball.com",
		}},
		cli.Config.Verbose,
		os.Stdout,
		os.Stderr,
	).CrawlOnefootball(cli.Config.Limit)

	// And here's the view layer, which can display implementations of Storage
	view.Tabwriter{}.Show(os.Stdout, storage)
}
